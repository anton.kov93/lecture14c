﻿#include <iostream>
#include <string>

int main()
{
    std::string FullName = "Kovalev Anton Evgenevich";
    std::cout << "Full Name: " << FullName << "\n";
    std::cout << "Line Length: " << FullName.length() << "\n";
    std::cout << "First Symbol: " << FullName.front() << "\n";
    std::cout << "Last Symbol: " << FullName.back() << "\n";
    return 0;
}
